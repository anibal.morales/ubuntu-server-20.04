# UBUNTU SERVER 20.04
## IP


*Configuración IP*
```S
cd /etc/netplan
nano 00-installer-config.yaml
```

*Configuración*
```S
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      dhcp4: no
      addresses: [192.168.10.41/24]
      gateway4: 192.168.10.2
      nameservers:
        addresses: [8.8.8.8]
  version: 2
```
*Aplicar configuración*
```S
sudo netplan apply
```
*Ver IP*
```S
ip addr
```

## APACHE
*Instalación*
```S
sudo apt-get install apache2
```

## PHP
*Instalación*
```S
sudo apt-get install php7.4
```

## SqlSrv on PHP 7.4
```S
sudo apt update
sudo apt install php-pear php-dev
curl -s https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo bash -c "curl -s https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list"
sudo apt update
sudo ACCEPT_EULA=Y apt -y install msodbcsql17 mssql-tools
sudo apt -y install unixodbc-dev
sudo apt -y install gcc g++ make autoconf libc-dev pkg-config
sudo pecl install sqlsrv
sudo pecl install pdo_sqlsrv
sudo bash -c "echo extension=sqlsrv.so > /etc/php/7.4/mods-available/sqlsrv.ini"
sudo ln -s /etc/php/7.4/mods-available/sqlsrv.ini /etc/php/7.4/apache2/conf.d/sqlsrv.ini
sudo ln -s /etc/php/7.4/mods-available/sqlsrv.ini /etc/php/7.4/cli/conf.d/sqlsrv.ini
sudo bash -c "echo extension=pdo_sqlsrv.so > /etc/php/7.4/mods-available/pdo_sqlsrv.ini"
sudo ln -s /etc/php/7.4/mods-available/pdo_sqlsrv.ini /etc/php/7.4/apache2/conf.d/pdo_sqlsrv.ini
sudo ln -s /etc/php/7.4/mods-available/pdo_sqlsrv.ini /etc/php/7.4/cli/conf.d/pdo_sqlsrv.ini
sudo /etc/init.d/apache2 restart
```